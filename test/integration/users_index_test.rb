require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin     = users(:michael)
    @non_admin = users(:archer)
  end

  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get users_path
    assert_select 'div.pagination'
    first_page_of_users = User.paginate(page: 1)
    first_page_of_users.each do |user|
      assert_select 'a', user.name
    end
    assert_select 'a', text: 'delete'
    user = first_page_of_users.first
    assert_difference 'User.count', -1 do
      delete user_path(user)
    end
    name     = "aaaa Fail"
    email    = "fail@example.com"
    password = "foobar"
    assert_difference 'User.count', 1 do
      post users_path, user: { name: name, email: email, password: password, password_confirmation: password }
    end
    assert_select "aaaa Fail", count: 0
    
    
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end
end

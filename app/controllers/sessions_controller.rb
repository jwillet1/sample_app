class SessionsController < ApplicationController
  EMAIL_LOGIN = /\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i
  
  def new
  end
  
  def create
    if EMAIL_LOGIN.match(params[:session][:email].downcase)
      @user = User.find_by(email: params[:session][:email].downcase)
    else
      @user = User.find_by(username: params[:session][:email].downcase)
    end
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        log_in @user
        params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
        redirect_back_or root_url
      else
        message  = "Account not activated. "
        message += "Check you email for the activation link"
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

end

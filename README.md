# Ruby on Rails Tutorial: sample application

The public version can be accessed using [this link](https://cryptic-spire-8268.herokuapp.com/).  

This is the sample application for
[*Learn Web Development: The Ruby on Rails
Tutorial*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).